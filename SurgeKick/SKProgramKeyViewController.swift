//
//  SKProgramKeyViewController.swift
//  SurgeKick
//
//  Created by Dalia Danila on 02/06/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import UIKit
import PopupDialog

class SKProgramKeyViewController: UIViewController {
    
    // The procedure for the specified car
    
    var procedure = Array<SKInstruction>()
    
    
    // Selected car
    
    var car: SKCar?

    
    // Current instruction number
    
    var step = 0
    
    
    // Outlets
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBOutlet weak var adviceLabel: UILabel!
    
    @IBOutlet weak var infoButton: UIBarButtonItem!
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        updateContent()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func handleNext(_ sender: Any) {
        
        // Increment step number
        
        self.step = self.step + 1;
        
        
        let totalSteps = procedure.count
        
        if self.step == totalSteps {
            
            // End of the process
            
            
            // Display success or error message
            
            let previousInstruction = procedure[step-1]
            
            if previousInstruction.instructionOutput == "1" {
                
                displaySuccessPopup()
            }
            else if previousInstruction.instructionOutput == "0" {
                
                displayErrorPopup()
            }
            
            return
        }
        
        let instruction = procedure[step]
        
        
        // Display HUD
        
        let hud = MBProgressHUD.showAdded(to: self.view, animated: true)
        
        hud.delegate = self
        
        hud.mode = MBProgressHUDMode(rawValue: Int((instruction.instructionLoadingIndicator?.loadingIndicatorMode)!))!;
        
        hud.label.text = (instruction.instructionLoadingIndicator?.loadingIndicatorMessage)!;
        
        hud.hide(animated: true, afterDelay: instruction.instructionLoadingIndicator?.loadingIndicatorDuration as! TimeInterval)
        
    }
    
    @IBAction func showInfo(_ sender: Any) {
        
        // Current instruction
        
        let instruction = procedure[step]
        
        // If it's iPad, display info using action sheet style
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            
            // Display alert with info
            
            let alert = UIAlertController(title: "Info", message: instruction.instructionInfo, preferredStyle: .actionSheet)
            
            alert.modalPresentationStyle = .popover
            
            let okAction = UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: .cancel)
            
            alert.addAction(okAction)
            
            if let presenter = alert.popoverPresentationController {
                presenter.barButtonItem = infoButton
            }
            present(alert, animated: true, completion: nil)
            
        }
            
            // Else, use the popup dialog
            
        else {
            
            // Prepare the popup
            
            let title = "Info"
            let message = instruction.instructionInfo
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: message, buttonAlignment: .horizontal, transitionStyle: .zoomIn, gestureDismissal: true)
            
            // Create second button
            let okButton = DefaultButton(title: "Ok", action: nil)
            
            // Add button to dialog
            popup.addButtons([okButton])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func handleCancel(_ sender: Any) {
        
        
        // Display alert with info
        
        let alert = UIAlertController(title: "Quit process", message: "Are you sure you want to cancel current process?", preferredStyle: .actionSheet)
        
        alert.modalPresentationStyle = .popover
        
        let yesAction = UIAlertAction(title: NSLocalizedString("Yes", comment: ""), style: .destructive) { action in
            
            self.dismiss(animated: true, completion: nil)
        }
        
        let noAction = UIAlertAction(title: NSLocalizedString("No", comment: ""), style: .cancel)
        
        alert.addAction(yesAction)
        alert.addAction(noAction)
        
        if let presenter = alert.popoverPresentationController {
            presenter.barButtonItem = infoButton
        }
        present(alert, animated: true, completion: nil)
    }
    
    func updateContent() {
        
        
        // Current instruction
        
        let instruction = procedure[step]
        
        
        let totalSteps = procedure.count
        
        
        // Update navigation title
        
        self.title = "Step \(self.step + 1)/\(totalSteps)"
        
        
        // Update progress bar
        
        progressView.setProgress(Float(Double(self.step + 1)/Double(totalSteps)), animated: true)
        
        
        // Update instruction text
        
        self.instructionLabel.text = instruction.instructionText
        
        
        // Update advice text
        
        self.adviceLabel.text = instruction.instructionAdvice
        
        
        // Set visibility for info bar button item
        
        if instruction.instructionInfo?.isEmpty == true {
            
            infoButton.isEnabled = false
        }
        else {
            
            infoButton.isEnabled = true
        }
    }
    
    func displaySuccessPopup() {
        
        let overlayAppearance = PopupDialogOverlayView.appearance()
        
        overlayAppearance.color       = UIColor.clear
        overlayAppearance.blurRadius  = 20
        overlayAppearance.blurEnabled = true
        overlayAppearance.liveBlur    = true
        overlayAppearance.opacity     = 0.5
        
        // Prepare the popup assets
        let title = "A W E S O M E"
        let message = "Your key has been successfully programmed."
        let image = UIImage(named: "Car Key")
        
        // Create the dialog
        let popup = PopupDialog(title: title, message: message, image: image)
        
        
        // Create 'Done' button
        let doneButton = CancelButton(title: "Done") {
            
            self.dismiss(animated: true, completion: nil)
        }
        
        // Add buttons to dialog
        popup.addButtons([doneButton])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
    
    func displayErrorPopup() {
        
        let overlayAppearance = PopupDialogOverlayView.appearance()
        
        overlayAppearance.color       = UIColor.clear
        overlayAppearance.blurRadius  = 20
        overlayAppearance.blurEnabled = true
        overlayAppearance.liveBlur    = true
        overlayAppearance.opacity     = 0.5
        
        // Prepare the popup assets
        
        let title = "Oh no!"
        let message = "Something has gone wrong..."
        let image = UIImage(named: "Error")
        
        // Create the dialog
        
        let popup = PopupDialog(title: title, message: message, image: image)
        
        // Create 'Try Again' button
        
        let tryAgainButton = DefaultButton(title: "Try Again") {
            
            self.step = 0
            
            self.updateContent()
        }
        
        // Create 'Cancel' button
        
        let cancelButton = CancelButton(title: "Cancel") {
            
            self.dismiss(animated: true, completion: nil)
        }
        
        // Add buttons to dialog
        popup.addButtons([tryAgainButton, cancelButton])
        
        // Present dialog
        self.present(popup, animated: true, completion: nil)
        
    }
}

extension SKProgramKeyViewController: MBProgressHUDDelegate {
    
    func hudWasHidden(_ hud: MBProgressHUD) {
        
        let previousInstruction = procedure[step - 1]
        
        if previousInstruction.instructionOutput == "VIN" {
            
            let title = "VIN number for \(car?.carMake! ?? "") \(car?.carModel! ?? ""), \(car?.carYears! ?? "") is VPN2747473394"
            
            // Create the dialog
            let popup = PopupDialog(title: title, message: nil, buttonAlignment: .horizontal, transitionStyle: .zoomIn, gestureDismissal: true)
            
            // Create second button
            let okButton = DefaultButton(title: "Ok", action: {
                
                self.updateContent()
            })
            
            // Add button to dialog
            popup.addButtons([okButton])
            
            // Present dialog
            self.present(popup, animated: true, completion: nil)
            
        }
        else {
            
            // Update UI
            
            updateContent()
            
        }
        
    }
    
}
