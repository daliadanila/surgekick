//
//  SKProcedureManager.swift
//  SurgeKick
//
//  Created by Dalia Danila on 19/06/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import Foundation

class SKProcedureManager {
    
    static let shared = SKProcedureManager()
    
    var allProcedures = Dictionary<String, Any>()
    
    private init() {
    
        // Read the plist file
        
        if let path = Bundle.main.path(forResource: "SKProcedures_demo", ofType: "plist") {
            
            self.allProcedures = NSDictionary(contentsOfFile: path)! as! Dictionary<String, Any>
            
            print(self.allProcedures)
        }
        
    }

    // Gets the procedure for a specific car reference
    
    func getProcedure(carReference: String) -> Array<SKInstruction>{
        
        let steps = self.allProcedures[carReference] as! Array<Dictionary<String, Any>>
        
        var procedure = Array<SKInstruction>()
        
        for step in steps {
            
            let instruction = SKInstruction.init(
                instructionText:   step["Instruction"] as! String,
                instructionAdvice: step["Advice"] as! String,
                instructionInfo:   step["Info"] as! String,
                instructionOutput: step["Output"] as! String,
                instructionLoadingIndicator: step["Loading Indicator"] as! Dictionary<String, Any>)
            
            procedure.append(instruction)
        }
    
        return procedure
    
    }
}
