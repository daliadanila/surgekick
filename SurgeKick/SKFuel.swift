//
//  SKFuel.swift
//  SurgeKick
//
//  Created by Dalia Danila on 31/05/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import Foundation

enum SKFuel: Int {
    
    case Petrol = 0
    
    case Diesel = 1
    
    case Hybrid = 2
}
