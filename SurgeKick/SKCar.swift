//
//  SKCar.swift
//  SurgeKick
//
//  Created by Dalia Danila on 31/05/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import UIKit

class SKCar {
    
    var carReference:   String?
    
    var carMake:        String?
    
    var carModel:       String?
    
    var carYears:       String?
    
    var carYear:        Int?
    
    var carFuel:        SKFuel?
    
    var carCountry:     String?
    
    
    init(carReference: String,
         carMake:      String,
         carModel:     String,
         carYears:     String,
         carYear:      Int,
         carFuel:      SKFuel,
         carCountry:   String) {
        
        self.carReference = carReference
        
        self.carMake      = carMake
        
        self.carModel     = carModel
        
        self.carYears     = carYears
        
        self.carYear      = carYear
        
        self.carFuel      = carFuel
        
        self.carCountry   = carCountry
    }

}
