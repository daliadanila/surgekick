//
//  SKStep.swift
//  SurgeKick
//
//  Created by Dalia Danila on 19/06/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import Foundation

class SKInstruction {
    
    var instructionText:             String?
    
    var instructionAdvice:           String?
    
    var instructionInfo:             String?
    
    var instructionOutput:           String?
    
    var instructionLoadingIndicator: SKLoadingIndicator?
    
    
    init(instructionText:             String,
         instructionAdvice:           String,
         instructionInfo:             String,
         instructionOutput:           String,
         instructionLoadingIndicator: Dictionary <String, Any>) {
        
        
        self.instructionText             = instructionText
        
        self.instructionAdvice           = instructionAdvice
        
        self.instructionInfo             = instructionInfo
        
        self.instructionOutput           = instructionOutput
        
        self.instructionLoadingIndicator = SKLoadingIndicator.init(
            loadingIndicatorMessage: instructionLoadingIndicator["Message"] as! String,
            loadingIndicatorMode: instructionLoadingIndicator["Mode"] as! NSNumber,
            loadingIndicatorDuration: instructionLoadingIndicator["Duration"] as! NSNumber,
            loadingIndicatorCanCancel: instructionLoadingIndicator["Can Cancel"] as! Bool)
    }
}
