//
//  SKLoadingIndicator.swift
//  SurgeKick
//
//  Created by Dalia Danila on 19/06/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import Foundation

class SKLoadingIndicator {
    
    var loadingIndicatorMessage:   String?
    
    var loadingIndicatorMode:      NSNumber?
    
    var loadingIndicatorDuration:  NSNumber? // in seconds
    
    var loadingIndicatorCanCancel: Bool?
    
    init(loadingIndicatorMessage:   String,
         loadingIndicatorMode:      NSNumber,
         loadingIndicatorDuration:  NSNumber,
         loadingIndicatorCanCancel: Bool) {
        
        self.loadingIndicatorMessage   = loadingIndicatorMessage
        
        self.loadingIndicatorMode      = loadingIndicatorMode
        
        self.loadingIndicatorDuration  = loadingIndicatorDuration
        
        self.loadingIndicatorCanCancel = loadingIndicatorCanCancel
    }
}
