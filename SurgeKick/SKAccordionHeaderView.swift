//
//  SKAccordionHeaderView.swift
//  SurgeKick
//
//  Created by Dalia Danila on 30/05/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import UIKit
import FZAccordionTableView

class SKAccordionHeaderView: FZAccordionTableViewHeaderView {
    
    @IBOutlet weak var accordionHeaderIcon: UIImageView!
    
    @IBOutlet weak var accordionHeaderLabel: UILabel!
    
    static let kDefaultAccordionHeaderViewHeight: CGFloat = 44.0;
    static let kAccordionHeaderViewReuseIdentifier = "AccordionHeaderViewReuseIdentifier";
    
}
