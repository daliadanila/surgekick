//
//  IntroViewController.swift
//  SurgeKick
//
//  Created by Dalia Danila on 04/05/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import UIKit

class SKIntroViewController: UIViewController {
    
    @IBOutlet weak var getStartedButton: UIButton!
    
    let gradientLayer: CAGradientLayer = {
        
        // Create gradient layer
        
        let layer = CAGradientLayer()
        
        
        // Set colors for the gradient
        
        let colorTop = UIColor(red: 112.0/255.0, green: 219.0/255.0, blue: 155.0/255.0, alpha: 1.0).cgColor
        
        let colorBottom = UIColor(red: 86.0/255.0, green: 197.0/255.0, blue: 238.0/255.0, alpha: 1.0).cgColor
        
        layer.colors = [colorTop, colorBottom]
        
        
        // Set start/end points and the frame for the gradient.
        
        layer.startPoint = CGPoint(x: 0.0, y: 0.5)
        
        layer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        
        // Add same rounded corners value for the layer like the one for the button
        
        layer.cornerRadius = 17

        
        return layer
        
    }()

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        setupGradientButton()
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Styling methods
    
    func setupGradientButton() {
        
        
        // Set frame for layer.
        
        gradientLayer.frame = getStartedButton.bounds
        
        
        // Add gradient layer to button
        
        getStartedButton.layer.addSublayer(gradientLayer)
    }

}

