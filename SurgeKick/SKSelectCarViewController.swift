//
//  SelectCarTableViewController.swift
//  SurgeKick
//
//  Created by Dalia Danila on 30/05/2017.
//  Copyright © 2017 Dalia Danila. All rights reserved.
//

import UIKit
import FZAccordionTableView

class SKSelectCarViewController: UIViewController {
    
    // MARK : - Proprties
    
    static fileprivate let kTableViewCellReuseIdentifier = "Cell"
    
    var filteredCars = [SKCar]()
    
    var makes : Array <String> = ["Ford", "Kia", "Toyota", "Volvo"]
    
    var cars : Array <Array <SKCar> > = [
        
        [SKCar.init(carReference: "0001", carMake: "Ford", carModel: "Fiesta", carYears: "2002 - 2008", carYear: 2007, carFuel: SKFuel.Petrol, carCountry: "Germany"),
                  
    
         SKCar.init(carReference: "0002", carMake: "Ford", carModel: "Fiesta", carYears: "2002 - 2008", carYear: 2006, carFuel: SKFuel.Petrol, carCountry: "Mexic")],
        
        [SKCar.init(carReference: "0003", carMake: "Kia", carModel: "Optima", carYears: "2009 - 2010", carYear: 2010, carFuel: SKFuel.Diesel, carCountry: "UK")],
        
        [SKCar.init(carReference: "0004", carMake: "Toyota", carModel: "Prius", carYears: "2009 - Present", carYear: 2015, carFuel: SKFuel.Hybrid, carCountry: "UK")],
        
        [SKCar.init(carReference: "0005", carMake: "Volvo", carModel: "V40", carYears: "2008 - 2011", carYear: 2010, carFuel: SKFuel.Petrol, carCountry: "Sweden")]
    ]
    
    // MARK : - Outlets
    
    @IBOutlet weak var showAllButton: UIBarButtonItem!
    
    @IBOutlet fileprivate weak var tableView: FZAccordionTableView! {
        
        didSet {
            
            tableView.allowMultipleSectionsOpen = true
            
            tableView.allowsMultipleSelection = true
            
            tableView.register(UINib(nibName: "SKAccordionHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: SKAccordionHeaderView.kAccordionHeaderViewReuseIdentifier)
            
            tableView.tableHeaderView = searchController.searchBar
        }
    }
    
    let searchController: UISearchController = {
        
        let searchController = UISearchController(searchResultsController: nil)
        
        searchController.definesPresentationContext = true
        
        searchController.dimsBackgroundDuringPresentation = false
        
        
        return searchController
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchController.searchResultsUpdater = self
        
        searchController.searchBar.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        self.showAllModels(self)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    @IBAction func showAllModels(_ sender: Any) {
        
        self.tableView.beginUpdates()
        
        if showAllButton.title == "Show All" {
            
            showAllButton.title = "Hide All"
            
            for i in 0..<self.tableView.numberOfSections {
                
                if self.tableView.isSectionOpen(i) == false {
                    
                    self.tableView.toggleSection(i)
                }
            }
            
        }
        else {
            
            showAllButton.title = "Show All"
            
            for i in 0..<self.tableView.numberOfSections {
                
                if self.tableView.isSectionOpen(i) == true {
                    
                    self.tableView.toggleSection(i)
                }
            }
            
        }
        
        self.tableView.endUpdates()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "startProcess" {
            
            let selectedCar = sender as! SKCar
            
            // Get selected car reference
            
            let carReference = selectedCar.carReference
            
            
            // Get steps from plist file
            
            let procedure = SKProcedureManager.shared.getProcedure(carReference: carReference!)
            
            
            let nc = segue.destination as! UINavigationController
            
            let vc = nc.viewControllers[0] as! SKProgramKeyViewController
            
            vc.procedure = procedure
            
            vc.car = selectedCar
        }
    }
}

// MARK: - Table view delegate methods

extension SKSelectCarViewController : UITableViewDelegate, UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            return 1
        }
        
        return makes.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            return filteredCars.count
        }
 
        return cars[section].count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            return 0
        }
        
        return SKAccordionHeaderView.kDefaultAccordionHeaderViewHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        
        return self.tableView(tableView, heightForHeaderInSection:section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: SKSelectCarViewController.kTableViewCellReuseIdentifier, for: indexPath)
        
        let car: SKCar
        
        // Check if search is active
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            car = filteredCars[indexPath.row]
            
        }
        else {
            
            car = cars[indexPath.section][indexPath.row]
            
        }
        
        cell.textLabel?.text = car.carMake! + " " + car.carModel! + ", " + car.carYears! + ", " + car.carCountry!
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let car: SKCar
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            car = filteredCars[indexPath.row]
            
        }
        else {
            
            car = cars[indexPath.section][indexPath.row]
            
        }
        
        confirmCar(car)
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if searchController.isActive && searchController.searchBar.text != "" {
            
            return nil
        }
        
        
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: SKAccordionHeaderView.kAccordionHeaderViewReuseIdentifier) as! SKAccordionHeaderView
       
            headerView.accordionHeaderLabel.text = makes[section]
            
            return headerView
    }
    
}

// MARK: - Accordion table view delegate methods

extension SKSelectCarViewController : FZAccordionTableViewDelegate {
    
    func tableView(_ tableView: FZAccordionTableView, willOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        let accordionHeaderView = header as! SKAccordionHeaderView
        
        accordionHeaderView.accordionHeaderIcon.image = UIImage(named: "Forward")
    }
    
    func tableView(_ tableView: FZAccordionTableView, didOpenSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, willCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
        let accordionHeaderView = header as! SKAccordionHeaderView
        
        accordionHeaderView.accordionHeaderIcon.image = UIImage(named: "Expand")
    }
    
    func tableView(_ tableView: FZAccordionTableView, didCloseSection section: Int, withHeader header: UITableViewHeaderFooterView?) {
        
    }
    
    func tableView(_ tableView: FZAccordionTableView, canInteractWithHeaderAtSection section: Int) -> Bool {
        return true
    }
}

// MARK: - Searching and filtering methods

extension SKSelectCarViewController {
    
    func filterContentForSearchText(_ searchText: String) {
        
        // Filter the array using the filter method
        
        if cars.isEmpty {
            
            filteredCars = []
            
            return
        }

        self.filteredCars.removeAll()
        
        for make in cars {
            
            for car in make {
                
                if ((car.carMake?.lowercased().contains(searchText.lowercased()))! ||
                                (car.carModel?.lowercased().contains(searchText.lowercased()))!   ||
                                (car.carYears?.lowercased().contains(searchText.lowercased()))! ||
                    (car.carCountry?.lowercased().contains(searchText.lowercased()))!) {
                    
                    self.filteredCars.append(car)
                }
                
            }
        }
        
        tableView.reloadData()
    }
    
    func confirmCar(_ car: SKCar) {
        
        let alert = UIAlertController(title: "Confirm Details", message: "Are you sure you want to program key for \(car.carMake!) \(car.carModel!), \(car.carYears!) ?", preferredStyle: UIAlertControllerStyle.alert);
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil));
        
        alert.addAction(UIAlertAction(title: "Yes", style: UIAlertActionStyle.default, handler: {(action:UIAlertAction) in
            
            // Start programming key process
            
            self.performSegue(withIdentifier: "startProcess", sender: car)
            
        }));
        
        present(alert, animated: true, completion: nil);
    }
}

// MARK: - UISearchBar delegate methods

extension SKSelectCarViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        filterContentForSearchText(searchText)
    }
}

// MARK: - UISearchResultsUpdating delegate methods

extension SKSelectCarViewController: UISearchResultsUpdating {

    func updateSearchResults(for searchController: UISearchController) {
        
        filterContentForSearchText(searchController.searchBar.text!)
    }
}
